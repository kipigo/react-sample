import React, { Suspense, lazy } from "react";
import { Provider } from "react-redux";
import { Router, Route, Switch } from "react-router-dom";
import "antd/dist/antd.css";
import Main from "./layout/Main";
import store from "./store/store";

import Desk from "./components/desk/Desk";
import { history } from "./store/store";
import Setup from "./components/setup/Setup";

function App() {
  return (
 
    <Provider store={store()}>
      <Router history={history}>
        <Switch>
          <Route
            exact
            path={"/login"}
            component={WaitingComponent(
              lazy(() => import("./components/login/Login"))
            )}
          />
        
          <Main>
            <Switch>
              <Route exact path={"/desk"} component={Desk} />
              <Route exact path={"/setup"} component={Setup} />
            </Switch>
          </Main>
     
        </Switch>
      </Router>
    </Provider>
  );
}

function WaitingComponent(Component) {
  return props => (
    <Suspense fallback={<div>Loading...</div>}>
      <Component {...props} />
    </Suspense>
  );
}

export default App;
