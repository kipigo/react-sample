import React from "react";
import { Layout, Button } from "antd";
import { history } from "../store/store";
import { connect } from "react-redux";
import { color } from "../assets/color";
import AuthAction from "../components/login/AuthAction";
import ReactCountdownClock from "react-countdown-clock";
const { Header } = Layout;

class Main extends React.Component {
  state = {
    collapsed: false
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };

  redirectToLogin() {
    history.push("/login");
  }

  render() {
    const { children, requestLogout } = this.props;
    const user = localStorage.getItem("user");
    return (
      <Layout>
        <div></div>
        <Header style={{ backgroundColor: 'white' , boxShadow:`0px 10px 24px -4px rgba(0,0,0,0.27)` , zIndex:2 }}>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              margin: 15
            }}
          >
            <div style={{ display: "flex" }}>
              <Button type="link"  onClick={() => history.push("/desk")}>Desk</Button>
              <Button type="link"  onClick={() => history.push("/setup")}>Setup Page</Button>
            </div>
            <div style={{ display: "flex" }}>
              <Button type="link"  onClick={() => requestLogout()}>Logout</Button>
            </div>
          </div>
        </Header>
        <div style={{backgroundColor: 'white', position: 'absolute', top: 90, left: 0, right: 0, bottom: 0}}>
  
          <div style={{ display: "flex", justifyContent: "center", }}>
            {user ? (
              children
            ) : (
              <div style={{ justifyContent: "center", marginTop: '30vh' }}>
                    <div style={{ display: "flex", justifyContent: "center", margin: '2vh' }}>
                  <ReactCountdownClock
                    seconds={5}
                    onComplete={()=> history.push('/login')}
                    alpha={0.9}
                    size={50}
                  />
                </div>
                <div style={{textAlign: 'center'}} >
                <div style={{fontWeight: 'bold', fontSize: 12}}>You cannot view this item</div>
                <div style={{ fontSize: 10}}>You will be redirected to login page</div>
                </div>
              </div>
            )}
          </div>
        </div>
        
      </Layout>
    );
  }
}

export default connect(
  state => ({}),
  dispatch => ({
    requestLogout() {
      dispatch(AuthAction.RequestLogut());
    }
  })
)(Main);
