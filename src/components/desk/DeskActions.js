import { DeskServices } from "../../services";
import {notification } from 'antd'
import {history} from '../../store/store'



export const DeskActionTypes = {
  DESK_REQUEST: "@@DESK_REQUEST",
  DESK_STOP: "@@DESK_STOP",
  DESK_SUCCESS: "@@DESK_SUCCESS",
  DESK_FAIL: "@@DESK_FAIL",
  DESK_CLEAR: "@@DESK_CLEAR"
};

export default {
  getIssues() {
    return async dispatch => {
      try {
        dispatch({
          type: DeskActionTypes.DESK_REQUEST,
        });
        let issues = await DeskServices.getIssues();
        dispatch({
          type: DeskActionTypes.DESK_STOP,
        });
        dispatch({
          type: DeskActionTypes.DESK_SUCCESS,
          payload: issues
        });
       
      } catch (e) {
        dispatch({
          type: DeskActionTypes.DESK_FAIL,
          error: e
        });
      }
    };
  },
  getCount(value) {
    return async dispatch => {
      try {
        let issues = await DeskServices.getCount(value);
        return issues.length;
      } catch (e) {
        dispatch({
          type: DeskActionTypes.DESK_FAIL,
          error: e
        });
      }
    };
  },
  postIssue(subject, description, creation, name) {
    return async dispatch => {
      try {
        dispatch({
          type: DeskActionTypes.DESK_REQUEST,
        });
        await DeskServices.postIssues(subject, description, creation, name);
        dispatch({
          type: DeskActionTypes.DESK_STOP,
        });
        notification.success({
          message: 'Saved'
        })
        history.push('/desk')
      } catch (e) {
        dispatch({
          type: DeskActionTypes.DESK_STOP,
        });
        notification.error({
          message: e.message
        })
        let issues = await DeskServices.getIssues();
        dispatch({
          type: DeskActionTypes.DESK_SUCCESS,
          payload: issues
        });
        history.push('/desk')
        // dispatch({
        //   type: DeskActionTypes.DESK_FAIL,
        //   error: e
        // });
      }
    };
  }
};
