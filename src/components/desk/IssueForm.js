import React from "react";
import { Input, Modal } from "antd";
import { connect, } from "react-redux";
import "antd/dist/antd.css";
import DeskActions from "./DeskActions";

const style = {
  input: {
    margin: "1vh",
    fontSize: 10
  },
  flexCenter: {
    display: "flex",
    justifyContent: "center",
    margin: "1vh"
  }
};

class IssueForm extends React.Component {
  

  state = {
    subject: null,
    description: null,
    creation: new Date().toDateString(),
    name: null,
    description: null
  };

  postIssue(){
      const { subject, description, creation, name} = this.state
      const { postIssue} = this.props
      postIssue(subject, description, creation, name)
  }

  render() {
    const { history, authRequest, visible , onOk, onCancel} = this.props;
    const { subject, description, creation, name} = this.state
    return (
      <Modal 
      visible={visible} 
      onOk={()=>onOk(subject, description, creation, name)}
      style={{margin: 'auto'}}
      onCancel={onCancel}
      >
      <div style={{ width: 400,}}>
        <Input
          placeholder='Subject'
          onChange={value => this.setState({ subject: value.target.value })}
          style={style.input}
        />
         <Input
             placeholder='Name'
          onChange={value => this.setState({ name: value.target.value })}
          style={style.input}
        />
        <Input.TextArea
          placeholder='Description'
          onChange={value => this.setState({ description: value.target.value })}
          style={style.input}
        />
        {/* <div style={style.flexCenter}>
          <Button onClick={() => this.postIssue(subject, description, creation, name) } type="primary">
            Submit
          </Button>
        </div> */}
      </div>
      </Modal>
    );
  }
}

export default connect(
  (state) => ({

  }),
  dispatch => ({
    postIssue(subject, description, creation, name){
      dispatch(DeskActions.postIssue(subject, description, creation, name))
    }
  })
)(IssueForm);
