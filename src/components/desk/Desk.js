import React from "react";
import { connect } from "react-redux";
import { Chart, Axis, Tooltip, Geom } from "bizcharts";
import { Table, Button, Modal, Statistic } from "antd";
import "antd/dist/antd.css";
import DeskActions from "./DeskActions";
import IssueForm from "./IssueForm";

let columns = [
  {
    title: "name ID",
    dataIndex: "name",
    key: "name"
  },
  {
    title: "subject",
    dataIndex: "subject",
    key: "subject"
  },
  {
    title: "creation",
    dataIndex: "creation",
    key: "creation"
  },
  {
    title: "status",
    dataIndex: "status",
    key: "status"
  },
  {
    title: "owner",
    dataIndex: "owner",
    key: "owner"
  },
  {
    title: "Type",
    dataIndex: "issue_type",
    key: "issue_type"
  },
  {
    title: "",
    dataIndex: "actions",
    key: "actions"
  }
];

class Desk extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    visible: false
  };

  componentDidMount() {
    const { getIssues } = this.props;
    getIssues();
  }

  async getCount(value) {
    const { getCount } = this.props;
    let count = await getCount(value);
    return count
  }

  createCharData() {
    let data = [];
    let issue = this.getIssueTypes();
    issue.map(async (item, key)=> {
      let count =  this.getCount(item)
      data.push({
        x: item,
        y: key+1
      });
    });
    console.log(data);
    return data;
  }

  getIssueTypes() {
    let issueTypes = [];
    const { list } = this.props;
    let fields = list
      ? list.map(item => {
          if (item.issue_type) {
            issueTypes.push(item.issue_type);
          }
        })
      : [];
    let unique = [...new Set(issueTypes)];
    return unique;
  }

  renderFields() {
    const { list } = this.props;
    let fields = list ? list : [];
    fields.map(item => {
      item["actions"] = (
        <div>
          <Button>Delete</Button>
          <Button>Save</Button>
        </div>
      );
    });

    return fields;
  }

  render() {
    const { list, loading, postIssue, subjects } = this.props;

    return (
      <div
        style={{
          flex: 12,
          justifyContent: "center",
          margin: "2vh",
          display: "flex"
        }}
      >
        <div style={{ marginBottom: "2vh", flex: 2 }}>
          Issues
          <div>Add</div>
        </div>

        <div style={{ marginBottom: "2vh", flex: 9 }}>
          {/* <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              marginLeft: 10,
              marginRight: 10,
              marginBottom: 20
            }}
          >
            <Chart height={400} data={this.createCharData()} forceFit>
              <Axis name="month" />
              <Axis
                name="x"
                label={{ formatter: val => `${val}°C` }}
              />
              <Tooltip crosshairs={{ type: "y" }} />
              <Geom
                type="line"
                position="month*temperature"
                size={2}
                color={"city"}
              />
            </Chart>
          </div> */}
          <div>
            <Button
              onClick={() => this.setState({ visible: true })}
              type="primary"
              style={{ marginBottom: 10 }}
            >
              Add Issue
            </Button>
          </div>
          <Table
            pagination={{
              pageSize: 10,
              position: "bottom",
              size: "small",
              showQuickJumper: true
            }}
            loading={loading}
            // pagination={false}
            size="small"
            dataSource={this.renderFields()}
            columns={columns}
          />

          <IssueForm
            visible={this.state.visible}
            onCancel={() => this.setState({ visible: false })}
            onOk={(subject, description, creation, name) =>
              this.props.postIssue(subject, description, creation, name)
            }
          />
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    list: state.desk.issues,
    loading: state.desk.loading
  }),
  dispatch => ({
    getIssues() {
      dispatch(DeskActions.getIssues());
    },
    postIssue(value) {
      dispatch(DeskActions.postIssue(value));
    },
    getCount(value) {
      return dispatch(DeskActions.getCount(value));
    }
  })
)(Desk);
