import React from "react";
import { Input, Button } from "antd";
import { connect, } from "react-redux";
import AuthActions from './AuthAction'


import "antd/dist/antd.css";

const style = {
  input: {
    margin: "1vh",
    fontSize: 10
  },
  flexCenter: {
    display: "flex",
    justifyContent: "center",
    margin: "1vh"
  }
};

class Login extends React.Component {
  state = {
    password: null,
    username: null
  };

  render() {
    const { history, authRequest  } = this.props;
    const { username, password  } = this.state;
    return (
      <div style={{ width: 400, margin: "30vh auto" }}>
        <Input
          onChange={value => this.setState({ username: value.target.value })}
          style={style.input}
        />
        <Input.Password
          onChange={value => this.setState({ password: value.target.value})}
          style={style.input}
        />
        <div style={style.flexCenter}>
          <Button onClick={() => authRequest(username, password) } type="primary">
            Submit
          </Button>
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => ({

  }),
  dispatch => ({
    authRequest(username, password){
      dispatch(AuthActions.RequestAuth(username, password))
    }
  })
)(Login);
