
import { AuthServices }  from "../../services";
import { history } from "../../store/store";
export const AuthActionTypes = {
    'AUTH_REQUEST': '@@AUTH_REQUEST',
    'AUTH_SUCCESS': '@@AUTH_SUCCESS',
    'AUTH_FAIL': '@@AUTH_FAIL',
    'AUTH_CLEAR': '@@AUTH_CLEAR'
}

export default ({
    RequestAuth(username, password) {
        return async dispatch => {
          try {
            dispatch({
                type: AuthActionTypes.AUTH_REQUEST,
              });
            let user = await AuthServices.RequestAuth(username, password);
            
            dispatch({
                type: AuthActionTypes.AUTH_SUCCESS,
                payload: user
              });
            history.push('/desk')
          } catch (e) {
            dispatch({
                type: AuthActionTypes.AUTH_FAIL,
                error: e
              }); 
          }
        };
      },
      RequestLogut() {
        return async dispatch => {
          try {
            dispatch({
                type: AuthActionTypes.AUTH_REQUEST,
              });
            await AuthServices.RequestLogout();
            dispatch({
                type: AuthActionTypes.AUTH_CLEAR,
              });
            history.push('/login')
          } catch (e) {
            dispatch({
                type: AuthActionTypes.AUTH_FAIL,
                error: e
              }); 
          }
        };
      },
})