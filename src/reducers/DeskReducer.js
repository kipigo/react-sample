import {DeskActionTypes} from '../components/desk/DeskActions'

const initialState = {
    issues : [],
    loading: false, 
    error: ''
}

export default (state = initialState, action) => {

    switch (action.type) {
    case DeskActionTypes.DESK_REQUEST:
      return {
       loading: true,
      };
    case DeskActionTypes.DESK_SUCCESS:
        return {
        issues: action.payload
    };
    case DeskActionTypes.DESK_STOP:
        return {
        loading: false,
    };
    case DeskActionTypes.DESK_FAIL:
        return {
        error : action.payload,
        };
    case DeskActionTypes.DESK_CLEAR:
        return {
            initialState
    };
    default:
      return state
    }
   }