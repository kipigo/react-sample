import {AuthActionTypes} from '../components/login/AuthAction'


const initialState = {
    user : {},
    loading: false, 
    error: ''
}

export default (state = initialState, action) => {

    switch (action.type) {
    case AuthActionTypes.AUTH_REQUEST:
      return {
       loading: true,
       user : {},
      };
    case AuthActionTypes.AUTH_SUCCESS:
        return {
        loading: false,
        user: action.payload
    };
    case AuthActionTypes.AUTH_FAIL:
        return {
        loading: false,
        error : action,
        };
    case AuthActionTypes.AUTH_CLEAR:
        return {
            initialState
    };
    default:
      return state
    }
   }