import { combineReducers } from 'redux'
import { routerReducer } from "react-router-redux";
import AuthReducer from './AuthReducer'
import DeskReducer from './DeskReducer'

export default  combineReducers({
  router: routerReducer,
  auth: AuthReducer,
  desk: DeskReducer
})