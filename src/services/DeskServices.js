import axios from "axios";
import { authHeader } from "./header";
import { argumentPlaceholder } from "@babel/types";


class DeskServices {
    unique = (value, index, self) => {
        return self.indexOf(value) === index
    }

  static async getIssues() {
    let subjects = [];
    let iss = await axios.get(
      `https://cors-anywhere.herokuapp.com/https://demo-erp.ubx.ph/api/resource/Issue/?fields=["name","subject","creation","status","owner","issue_type"]&limit_page_length=100
    `,
      { headers: authHeader() }
    );
    return iss.data.data;
  }

  static async getCount(type) {
    let iss = await axios.get(
      `https://cors-anywhere.herokuapp.com/https://demo-erp.ubx.ph/api/resource/Issue/?fields=["name","subject","creation","status","owner","issue_type"]&filters=[["Issue","issue_type","=","${type}"]]
    `,
      { headers: authHeader() }
    );
    return iss.data.data
  }

  static async postIssues(subject, description, creation, name) {
    let param = {
      doctype: "Issue",
      subject: subject,
      name: name,
      naming_series: "ISS-.YYYY.-",
      company: "UBX",
      description: description,
      opening_date: new Date(),
      opening_time: new Date()
    };
    let fd = new FormData();
    fd.append("data", JSON.stringify(param));
    await axios.post(
      `https://cors-anywhere.herokuapp.com/https://demo-erp.ubx.ph/api/resource/Issue`,
      fd,
      { headers: authHeader() }
    );
  }
}

export default DeskServices;
